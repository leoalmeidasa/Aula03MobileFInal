function inc(speed, inc) {
    if (inc === void 0) { inc = 1; }
    return speed + inc;
}
console.log("inc (5, 1)=" + inc(5, 1));
console.log("inc (5) = " + inc(5));
